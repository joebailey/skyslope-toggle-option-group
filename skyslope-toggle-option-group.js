import { LitElement, html } from '@polymer/lit-element';
import 'skyslope-toggle-option/skyslope-toggle-option.js';

class SkySlopeToggleOptionGroup extends LitElement {
  static get properties() {
    return {
      options: {type: Array},
      value: {type: String, reflect: true},
      multiselect: {type: Boolean, reflect: true},
    };
  }

  constructor() {
    super();
    this.options = [];
    this.value = '';
    this.multiselect = false;
  }

  handleChange(e) {
    console.log('skyslope-toggle-option-group: Caught change event from skyslope-toggle-option!', e);
    e.stopPropagation();
    this.value = e.detail.checked ? e.detail.value : '';
    this.dispatchEvent(
      new CustomEvent('change', {
        bubbles: true,
        composed: true,
        detail: {
          value: this.value,
        },
      })
    );
  }

  defaultStyles() {
    return html`
      <style>
        .toggle-option-container {
          display: inline-flex;
          width: 100%;
        }
      </style>
    `;
  }

  render() {
    return html`
      ${this.defaultStyles()}

      <section class="toggle-option-container">
        ${this.options.map(item =>
          html`
          <skyslope-toggle-option
            value="${item}"
            ?checked="${this.value === item}"
            @change="${this.handleChange}">${item}</skyslope-toggle-option>
          `)}
      </section>
    `;
  }
}

customElements.define('skyslope-toggle-option-group', SkySlopeToggleOptionGroup);
